//  File name   : Model.swift
//
//  Author      : Dung Vu
//  Created date: 5/7/20
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2020 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import UIKit

// MARK: -- Configure
public struct VatoPasswordTitle {
    public let text: String?
    public let font: UIFont
    public let color: UIColor
    public let numberOfLines: Int
    public let alignment: NSTextAlignment
    
    public init(use text: String?, font: UIFont, color: UIColor, numberOfLines: Int, alignment: NSTextAlignment) {
        self.text = text
        self.font = font
        self.color = color
        self.numberOfLines = numberOfLines
        self.alignment = alignment
    }
}

public typealias VatoPasswordItem<T> = (first: T, last: T)

public protocol VatoPasswordUIProtocol {
    var title: VatoPasswordItem<VatoPasswordTitle?> { get }
    var description: VatoPasswordItem<VatoPasswordTitle>? { get }
    
    var iconClose: UIImage? { get }
    var iconBack: UIImage? { get }
    
    // Use need suppport
    var useSupportPhone: Bool { get }
    
    // First: title password, Last: text sub password
    var passwordTitle: VatoPasswordItem<VatoPasswordTitle>? { get }
    var roundRadius: CGFloat { get }
    
    var phoneSupport: String { get }
    var lengthPassword: VatoPasswordItem<Int> { get }
    
    var size: CGSize { get }
    var spacingPassword: CGFloat { get }
    var colorPassword: VatoPasswordItem<UIColor> { get }
    var colorPhoneCall: UIColor { get }
}


// MARK: -- Result
public struct VatoPasswordVerifyResult {
    public let password: String?
    public let valid: Bool
}

// MARK: -- Type Verify
public enum VatoVerifyType: Int {
    // Only input not verify with BE
    case notVerify
    case createNew
    case changePin
    case forgotPassword
}
