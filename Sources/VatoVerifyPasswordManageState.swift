//  File name   : VatoVerifyPasswordManageState.swift
//
//  Author      : Dung Vu
//  Created date: 5/7/20
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2020 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import UIKit
import RxSwift
import RxCocoa
import VatoNetwork
import FwiCoreRX
import Alamofire

public enum VerifyPasswordError: Error {
    case notSameBeforePassword
    case server(error: Error)
}

enum VerifyPasswordState: Equatable {
    case new
    case verifyBEWith(old: String?, password: String?)
    case verifyNew(password: String?)
    case finished(password: String?)
    
    static func ==(lhs: VerifyPasswordState, rhs: VerifyPasswordState) -> Bool {
        switch (lhs, rhs) {
        case (.new, .new):
            return true
        default:
            return false
        }
    }
    
    var finished: Bool {
        switch self {
        case .finished:
            return true
        default:
            return false
        }
    }
    
    var currentPassword: String? {
        switch self {
        case .verifyBEWith(_ , let password):
            return password
        case .verifyNew(let password), .finished(let password):
            return password
        default:
           return nil
        }
    }
    
    func nextState(_ type: VatoVerifyType, password: String?) throws -> VerifyPasswordState? {
        switch self {
        case .new:
            switch type {
            case .notVerify:
                return .finished(password: password)
            case .createNew, .changePin, .forgotPassword:
                return .verifyNew(password: password)
            }
        case .verifyNew(let p):
            switch type {
            case .changePin:
                return .verifyBEWith(old: p, password: password)
            case .createNew:
                guard p == password else {
                    throw VerifyPasswordError.notSameBeforePassword
                }
                return .verifyBEWith(old: nil, password: password)
            case .forgotPassword:
                return .verifyBEWith(old: p, password: password)
            default:
                precondition(false, "Please implement \(type)")
                return nil
            }
        case .verifyBEWith:
            return .finished(password: password)
        default:
            return nil
        }
    }
}

protocol VatoVerifyPresenterProtocol: AnyObject {
    var textField: UITextField { get }
    var configUI: VatoPasswordUIProtocol { get }
    var itemsView: [VatoPasswordItemView] { get }
    var eBack: Observable<Void> { get }
    
    func handlerError(e: VerifyPasswordError)
}

final class VatoVerifyPasswordManageState: NSObject, ActivityTrackingProtocol {
    private let token: Observable<String>
    private let type: VatoVerifyType
    private lazy var mState: BehaviorRelay<VerifyPasswordState> = BehaviorRelay(value: .new)
    private weak var presenter: VatoVerifyPresenterProtocol?
    private (set) var verified: Bool = false
    private var maxLenght: Int
    private lazy var disposeBag = DisposeBag()
    private var loading: Bool = false
    private var disposeRequest: Disposable?
    
    var state: Observable<VerifyPasswordState> {
        return mState.distinctUntilChanged()
    }
    
    var eLoading: Observable<Bool> {
        return indicator.asObservable()
    }
    
    
    init(use token: Observable<String>, type: VatoVerifyType, presenter: VatoVerifyPresenterProtocol?) {
        self.token = token
        self.type = type
        self.maxLenght = presenter?.configUI.lengthPassword.first ?? 0
        self.presenter = presenter
        super.init()
        presenter?.textField.delegate = self
    }
    
    private func resetText() {
        presenter?.textField.text = nil
        presenter?.textField.sendActions(for: .valueChanged)
    }
    
    private func verifyPassword(old: String?, p: String?) {
        guard let p = p, !p.isEmpty else { return }
        switch type {
        case .createNew:
            let network = NetworkRequester(provider: NetworkTokenProvider(token: token))
            let params = ["pin": p]
            let router = VatoAPIRouter.customPath(authToken: "", path: "user/set_pin", header: nil, params: params, useFullPath: false)
            disposeRequest = network
                .request(using: router, decodeTo: OptionalIgnoreMessageDTO<String>.self, method: .post, encoding: JSONEncoding.default)
                .trackActivity(indicator).bind (onNext: { [weak self](result) in
                guard let wSelf = self else { return }
                switch result {
                case .failure(let e):
                    wSelf.presenter?.handlerError(e: .server(error: e))
                case .success(let res):
                    if let e = res.error {
                        wSelf.presenter?.handlerError(e: .server(error: e))
                    } else {
                        wSelf.verified = true
                        wSelf.mState.accept(.finished(password: p))
                    }
                }
            })
        case .changePin:
            let network = NetworkRequester(provider: NetworkTokenProvider(token: token))
            var params = ["newPin": p]
            params["oldPin"] = old
            let router = VatoAPIRouter.customPath(authToken: "", path: "user/change_pin", header: nil, params: params, useFullPath: false)
            disposeRequest = network
                .request(using: router, decodeTo: OptionalIgnoreMessageDTO<String>.self, method: .post, encoding: JSONEncoding.default)
                .trackActivity(indicator).bind (onNext: { [weak self](result) in
                guard let wSelf = self else { return }
                switch result {
                case .failure(let e):
                    wSelf.presenter?.handlerError(e: .server(error: e))
                case .success(let res):
                    if let e = res.error {
                        wSelf.presenter?.handlerError(e: .server(error: e))
                    } else {
                        wSelf.verified = true
                        wSelf.mState.accept(.finished(password: p))
                    }
                }
            })
        case .forgotPassword:
            let network = NetworkRequester(provider: NetworkTokenProvider(token: token))
            var params: [String: String] = [:]
            params["resetToken"] = old
            params["newPin"] = p
            let router = VatoAPIRouter.customPath(authToken: "", path: "user/change_pin", header: nil, params: params, useFullPath: false)
            disposeRequest = network
                .request(using: router, decodeTo: OptionalIgnoreMessageDTO<String>.self, method: .post, encoding: JSONEncoding.default)
                .trackActivity(indicator).bind (onNext: { [weak self](result) in
                guard let wSelf = self else { return }
                switch result {
                case .failure(let e):
                    wSelf.presenter?.handlerError(e: .server(error: e))
                case .success(let res):
                    if let e = res.error {
                        wSelf.presenter?.handlerError(e: .server(error: e))
                    } else {
                        wSelf.verified = true
                        wSelf.mState.accept(.finished(password: p))
                    }
                }
            })
        default:
            break
        }
    }
    
    deinit {
        disposeRequest?.dispose()
    }
    
    func prepare() {
        let event = presenter?.textField.rx.text
        event?.bind(onNext: { [weak self](text) in
            guard let wSelf = self else { return }
            let count = text?.count ?? 0
            wSelf.presenter?.itemsView.enumerated().forEach { $0.element.isSelected = $0.offset <= (count - 1)}
        }).disposed(by: disposeBag)
        
        event?.filter {[unowned self] in $0?.count == self.maxLenght }.debounce(.milliseconds(300), scheduler: MainScheduler.instance).bind(onNext: { [weak self] (n) in
            guard let wSelf = self else { return }
            do {
                guard let next = try wSelf.mState.value.nextState(wSelf.type, password: n) else {
                    return
                }
                wSelf.mState.accept(next)
            } catch let error as VerifyPasswordError {
                wSelf.presenter?.handlerError(e: error)
            } catch {
                print(error.localizedDescription)
            }
        }).disposed(by: disposeBag)
        
        state.bind { [weak self](state) in
            guard let wSelf = self else { return }
            switch state {
            case .new:
                wSelf.maxLenght = wSelf.presenter?.configUI.lengthPassword.first ?? 0
                wSelf.resetText()
            case .verifyNew:
                wSelf.maxLenght = wSelf.presenter?.configUI.lengthPassword.last ?? 0
                wSelf.resetText()
            case .verifyBEWith(let old, let new):
                wSelf.verifyPassword(old: old, p: new)
            default:
                break
            }
        }.disposed(by: disposeBag)
        
        eLoading.bind { [weak self] in
            guard let wSelf = self else { return }
            wSelf.loading = $0
        }.disposed(by: disposeBag)
        
        presenter?.eBack.bind(onNext: { [weak self](_) in
            guard let wSelf = self else { return }
            wSelf.mState.accept(.new)
        }).disposed(by: disposeBag)
    }
}

extension VatoVerifyPasswordManageState: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard !loading else { return false  }
        let text = textField.text ?? ""
        let new = (text as NSString).replacingCharacters(in: range, with: string)
        return new.count <= maxLenght
    }
}

