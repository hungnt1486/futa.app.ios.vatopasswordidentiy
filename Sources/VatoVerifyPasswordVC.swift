//  File name   : VatoVerifyPasswordVC.swift
//
//  Author      : Dung Vu
//  Created date: 5/7/20
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2020 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import UIKit
import SnapKit
import FwiCore
import FwiCoreRX
import RxSwift
import RxCocoa

public final class VatoVerifyPasswordVC: UIViewController, VatoVerifyPresenterProtocol {
    /// Class's public properties.
    public typealias HandlerError = (_ error: VerifyPasswordError) -> String
    public typealias LoadingBlock = (_ loading: Bool) -> ()
    public typealias ForgotPasswordBlock = (_ phone: String) -> ()
    
    private lazy var mContainerView = VatoVerifyBgView(use: configUI.roundRadius)
    internal lazy var textField: UITextField = UITextField(frame: .zero)
    private lazy var titleLabel = UILabel(frame: .zero)
    private lazy var subLabel = UILabel(frame: .zero)
    private lazy var lblError = UILabel(frame: .zero)
    private (set) var itemsView: [VatoPasswordItemView] = []
    private lazy var btnBack = UIButton(frame: .zero)
    
    var eBack: Observable<Void> {
        return btnBack.rx.tap.asObservable()
    }
    
    let configUI: VatoPasswordUIProtocol
    private let type: VatoVerifyType
    public private(set) lazy var disposeBag = DisposeBag()
    private lazy var event: PublishSubject<VatoPasswordVerifyResult> = PublishSubject()
    private let token: Observable<String>
    private let handlerError: HandlerError
    private let forgotBlock: ForgotPasswordBlock?
    private lazy var manageState = VatoVerifyPasswordManageState(use: token, type: type, presenter: self)
    private let loadingBlock: LoadingBlock?
    private lazy var tapGesture: UITapGestureRecognizer = {
        let t = UITapGestureRecognizer(target: nil, action: nil)
        mContainerView.addGestureRecognizer(t)
        return t
    }()
    
    init(configUI: VatoPasswordUIProtocol,
         type: VatoVerifyType,
         token: Observable<String>,
         loading: LoadingBlock?,
         handlerError: @escaping HandlerError,
         forgotBlock: ForgotPasswordBlock?)
    {
        self.token = token
        self.configUI = configUI
        self.type = type
        self.handlerError = handlerError
        self.loadingBlock = loading
        self.forgotBlock = forgotBlock
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View's lifecycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        visualize()
        setupRX()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
     
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
              self.view.frame.origin.y -= keyboardSize.height
          }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0 // Move view to original position
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textField.becomeFirstResponder()
        localize()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        textField.resignFirstResponder()
    }
    
    func handlerError(e: VerifyPasswordError) {
        let message = handlerError(e)
        lblError.text = message
    }

}

// MARK: View's event handlers
public extension VatoVerifyPasswordVC {
    override var prefersStatusBarHidden: Bool {
        return false
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}

public extension VatoVerifyPasswordVC {
    static func showVerify(on controller: UIViewController?,
                           config: VatoPasswordUIProtocol,
                           type: VatoVerifyType,
                           token: Observable<String>,
                           loading: LoadingBlock?,
                           forgotBlock: ForgotPasswordBlock?,
                           handlerError: @escaping HandlerError) -> Observable<VatoPasswordVerifyResult>
    {
        guard let controller = controller else {
            precondition(false, "Please check controller!!!!!!")
            return Observable.empty()
        }
        
        return Observable.create { (s) -> Disposable in
            let verifyVC = VatoVerifyPasswordVC(configUI: config, type: type, token: token, loading: loading, handlerError: handlerError, forgotBlock: forgotBlock)
            let dispose = verifyVC.event.take(1).subscribe(s)
            verifyVC.modalPresentationStyle = .overCurrentContext
            verifyVC.modalTransitionStyle = .crossDissolve
            controller.present(verifyVC, animated: true, completion: nil)
            return Disposables.create {
                dispose.dispose()
                verifyVC.dismiss(animated: true, completion: nil)
            }
        }
    }
}

extension VatoVerifyPasswordVC: KeyboardAnimationProtocol {
    public var containerView: UIView? {
        return mContainerView
    }
}

// MARK: Class's private methods
private extension VatoVerifyPasswordVC {
    private func localize() {
        // todo: Localize view's here.
    }
    private func visualize() {
        // todo: Visualize view's here.
        view.backgroundColor = #colorLiteral(red: 0.06666666667, green: 0.06666666667, blue: 0.06666666667, alpha: 0.4)
        textField >>> view >>> {
            $0.keyboardType = .numberPad
        }
        
        mContainerView >>> view >>> {
            $0.backgroundColor = .clear
            $0.snp.makeConstraints { (make) in
                make.left.right.bottom.equalToSuperview()
            }
        }
        
        let first = configUI.title.first
        let stackView = UIStackView(frame: .zero)
        
        stackView >>> mContainerView >>> {
            $0.distribution = .fill
            $0.axis = .vertical
            $0.alignment = .center
            $0.spacing = 4
            $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
            $0.setContentHuggingPriority(.required, for: .vertical)
            $0.snp.makeConstraints { (make) in
                make.top.equalTo(24)
                make.left.equalTo(16)
                make.right.equalTo(-16)
            }
        }
        
        titleLabel >>> {
            $0.text = first?.text
            $0.font = first?.font
            $0.textColor = first?.color
            $0.textAlignment = first?.alignment ?? .center
            $0.numberOfLines = first?.numberOfLines ?? 0
            $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
            $0.setContentHuggingPriority(.required, for: .vertical)
        }
        
        stackView.addArrangedSubview(titleLabel)
        
        if let d = configUI.description {
            let f = d.first
            subLabel >>> {
                $0.text = f.text
                $0.font = f.font
                $0.textColor = f.color
                $0.textAlignment = f.alignment
                $0.numberOfLines = f.numberOfLines
                $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
                $0.setContentHuggingPriority(.required, for: .vertical)
                
            }
            stackView.addArrangedSubview(subLabel)
        }
        let m = max(configUI.lengthPassword.first, configUI.lengthPassword.last)
        let views = (0..<m).map { _ -> VatoPasswordItemView in
           let v = VatoPasswordItemView(use: configUI.colorPassword.first, colorUnselect: configUI.colorPassword.last)
            v >>> {
                $0.snp.makeConstraints { (make) in
                    make.size.equalTo(configUI.size)
                }
            }
           return v
        }
        itemsView = views
        let vPassword = UIStackView(arrangedSubviews: views)
        
        if configUI.useSupportPhone {
            precondition(!configUI.phoneSupport.isEmpty, "Not empty phone!!!!")
            vPassword >>> mContainerView >>> {
                $0.distribution = .fill
                $0.axis = .horizontal
                $0.spacing = configUI.spacingPassword
                $0.snp.makeConstraints { (make) in
                    make.centerX.equalToSuperview()
                    make.top.equalTo(stackView.snp.bottom).offset(32)
                }
            }
            guard let pTitle = configUI.passwordTitle else {
                return precondition(false, "Config passwordTitle not nil !!!!!")
            }
            let f = pTitle.first
            let lblTitlePassword = UILabel(frame: .zero)
            lblTitlePassword >>> {
                $0.text = f.text
                $0.font = f.font
                $0.textColor = f.color
                $0.textAlignment = f.alignment
                $0.numberOfLines = f.numberOfLines
                $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
                $0.setContentHuggingPriority(.required, for: .vertical)
            }
            
            let s = pTitle.last
            let buttonPhone = UIButton(frame: .zero)
            buttonPhone >>> {
                let t = $0.titleLabel
                t?.font = s.font
                $0.setTitle(s.text, for: .normal)
                $0.setTitleColor(s.color, for: .normal)
            }
            buttonPhone.rx.tap.bind { [weak self](_) in
                guard let wSelf = self else { return }
                defer {
                    wSelf.forgotBlock?(wSelf.configUI.phoneSupport)
                }
                wSelf.event.onCompleted()
            }.disposed(by: disposeBag)
            
            let vPhone = UIStackView(arrangedSubviews: [lblTitlePassword, buttonPhone])
            vPhone >>> mContainerView >>> {
                $0.distribution = .fill
                $0.axis = .vertical
                $0.alignment = .center
                $0.snp.makeConstraints { (make) in
                    make.top.equalTo(vPassword.snp.bottom).offset(24)
                    make.left.equalTo(16)
                    make.right.equalTo(-16)
                    make.bottom.equalTo(-16)
                }
            }
            
        } else {
            vPassword >>> mContainerView >>> {
                $0.distribution = .fill
                $0.axis = .horizontal
                $0.spacing = configUI.spacingPassword
                $0.snp.makeConstraints { (make) in
                    make.centerX.equalToSuperview()
                    make.top.equalTo(stackView.snp.bottom).offset(32)
                    make.bottom.equalTo(-50)
                }
            }
        }
        
        lblError >>> mContainerView >>> {
            $0.numberOfLines = 2
            $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
            $0.setContentHuggingPriority(.required, for: .vertical)
            $0.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            $0.textColor = configUI.colorPassword.last
            $0.textAlignment = .center
            
            $0.snp.makeConstraints { (make) in
                make.top.equalTo(vPassword.snp.bottom).offset(4)
                make.left.equalTo(16)
                make.right.equalTo(-16)
            }
        }
        
        let btnClose = UIButton(frame: .zero)
        btnClose >>> mContainerView  >>> {
            $0.setImage(configUI.iconClose, for: .normal)
            $0.snp.makeConstraints { (make) in
                make.top.right.equalToSuperview()
                make.size.equalTo(CGSize(width: 56, height: 44))
            }
        }
        
        btnClose.rx.tap.bind { [weak self](_) in
            self?.event.onCompleted()
        }.disposed(by: disposeBag)
        
        btnBack >>> mContainerView >>> {
            $0.isHidden = true
            $0.setImage(configUI.iconBack, for: .normal)
            $0.snp.makeConstraints { (make) in
                make.top.left.equalToSuperview()
                make.size.equalTo(CGSize(width: 56, height: 44))
            }
        }
    }
    
    func setupRX() {
        manageState.prepare()
        setupKeyboardAnimation()
        tapGesture.rx.event.bind { [weak self](_) in
            guard let wSelf = self, !wSelf.textField.isFirstResponder else { return }
            wSelf.textField.becomeFirstResponder()
        }.disposed(by: disposeBag)
        
        manageState.state.filter { $0.finished }.observeOn(MainScheduler.asyncInstance).bind { [weak self](s) in
            guard let wSelf = self else { return }
            switch s {
            case .finished(let password):
                let r = VatoPasswordVerifyResult(password: password, valid: wSelf.manageState.verified)
                wSelf.event.onNext(r)
            default:
                break
            }
        }.disposed(by: disposeBag)
        
        manageState.eLoading.observeOn(MainScheduler.asyncInstance).bind(onNext: { [weak self] in
            guard let wSelf = self else { return }
            wSelf.loadingBlock?($0)
        }).disposed(by: disposeBag)
        
        manageState.state.bind(onNext: {[weak self] state in
            guard let wSelf = self else { return }
            switch state {
            case .finished:
                break
            case .new:
                wSelf.btnBack.isHidden = true
                wSelf.titleLabel.text = wSelf.configUI.title.first?.text
                wSelf.subLabel.text = wSelf.configUI.description?.first.text
                wSelf.lblError.text = ""
                let imax = wSelf.configUI.lengthPassword.first
                wSelf.itemsView.enumerated().forEach { (i) in
                    i.element.isHidden = i.offset >= imax
                }
            default:
                wSelf.btnBack.isHidden = false
                wSelf.titleLabel.text = wSelf.configUI.title.last?.text
                wSelf.subLabel.text = wSelf.configUI.description?.last.text
                wSelf.lblError.text = ""
                let imax = wSelf.configUI.lengthPassword.last
                wSelf.itemsView.enumerated().forEach { (i) in
                    i.element.isHidden = i.offset >= imax
                }
            }
        }).disposed(by: disposeBag)
    }
}
