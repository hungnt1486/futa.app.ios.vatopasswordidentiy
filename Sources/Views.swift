//  File name   : Views.swift
//
//  Author      : Dung Vu
//  Created date: 5/7/20
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2020 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import UIKit

// MARK: -- Item
final class VatoPasswordItemView: UIView {
    var isSelected: Bool = false {
        didSet { updateState() }
    }
    private let color: UIColor
    private let colorUnselect: UIColor
    init(use color: UIColor, colorUnselect: UIColor) {
        self.color = color
        self.colorUnselect = colorUnselect
        super.init(frame: .zero)
        visualize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func visualize() {
        layer.borderWidth = 1
        clipsToBounds = true
        
        updateState()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let h = bounds.height
        layer.cornerRadius = h / 2
    }
    
    private func updateState() {
        let c = isSelected ? color : .white
        let borderColor = isSelected ? color : colorUnselect
        layer.borderColor = borderColor.cgColor
        backgroundColor = c
    }
}

// MARK: -- View
final class VatoVerifyBgView: UIView {
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    private var mLayer: CAShapeLayer? {
        return layer as? CAShapeLayer
    }
    private let radius: CGFloat
    init(use radius: CGFloat) {
        self.radius = radius
        super.init(frame: .zero)
        backgroundColor = .clear
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func createBenzier() -> UIBezierPath {
        let benzier = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: radius, height: radius))
        return benzier
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let benzier = createBenzier()
        mLayer?.path = benzier.cgPath
        mLayer?.fillColor = UIColor.white.cgColor
    }
}
